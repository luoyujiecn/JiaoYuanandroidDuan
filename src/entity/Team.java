package entity;

public class Team {
	private int tid;			// 对应数据库中主键
	private int schoolID;	// 所属学校
	private String teamName;// 班级名
	private boolean Complete;// 是否结业
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public int getSchoolId() {
		return schoolID;
	}
	public void setSchoolId(int schoolID) {
		this.schoolID = schoolID;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public boolean Complete() {
		return Complete;
	}
	public void setComplete(boolean Complete) {
		this.Complete = Complete;
	}

}
