package entity;

public class Student {
	private int stuId;			// 对应于数据库中主键
	private String loginId;		// 登录id
	private String loginPsw;	// 登录密码
	private String realName;	// 真实姓名
	private String team;		// 所属班级
	private double integralScore;// 每日签到积分
	public int getStuId() {
		return stuId;
	}
	public void setStuId(int stuId) {
		this.stuId = stuId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getLoginPsw() {
		return loginPsw;
	}
	public void setLoginPsw(String loginPsw) {
		this.loginPsw = loginPsw;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public double getIntegralScore() {
		return integralScore;
	}
	public void setIntegralScore(double integralScore) {
		this.integralScore = integralScore;
	}

}
