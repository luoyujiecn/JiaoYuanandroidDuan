package close;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;

/**
 * Activity的池子
 * 
 * @author Shinelon
 * 
 */
public class ActivityPool {

	/**
	 * 存放的池子
	 */
	private static List<Activity> activities = new ArrayList<>();

	/**
	 * 添加Activity到池子中
	 * 
	 * @param activity
	 */
	public static void addActivity(Activity activity) {
		if (activity != null) {
			if(!activities.contains(activity)){
				activities.add(activity);
			}
		}
	}

	/**
	 * 关闭所有的界面。
	 */
	public static void closeAllActivity() {
		// 将池子中的所有界面都关闭
		for (Activity a : activities) {
			if (!a.isFinishing()) {
				a.finish();
			}
		}
		// 清空集合
		activities.clear();
	}

	/**
	 * 移除界面
	 * 
	 * @param activity
	 */
	public static void removeActivity(Activity activity) {
		activities.remove(activity);
	}

}
