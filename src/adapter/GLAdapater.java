package adapter;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.example.teacher.R;
import com.example.teacher.Tc_Team_Activity;

import entity.Team;

public class GLAdapater extends BaseAdapter{
	protected static final String MODE_PRIVATE = null;
	private List<Team> teams;
	private Context context;
	private Handler handler;
	
	String uu;
	public GLAdapater(List<Team> teams, Context context) {
		super();
		this.teams = teams;
		this.context = context;
	}
	
	public void setHandler(Handler handler){
		this.handler = handler;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return teams == null ? 0 : teams.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" }) @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Team  team = teams.get(position);
		//View v = LayoutInflater.from(context).inflate(R.layout.activity_tc_team_item, null);
		 final View vv = LayoutInflater.from(context).inflate(R.layout.activity_tc_team_item, null);
		Button btn_teamName = (Button) vv.findViewById(R.id.btn_teamName);
		Button btn_clk_guanlian = (Button) vv.findViewById(R.id.btn_clk_guanlian);
		btn_teamName.setText(team.getTeamName());
		 uu = team.getTid()+"";
		btn_clk_guanlian.setText("关联班级");
		
		btn_clk_guanlian.setClickable(true);
		btn_clk_guanlian.setTag(team);
		btn_clk_guanlian.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Team team = (Team)v.getTag();
				
				sendRequest(team.getTid()+"");
				GLAdapater.this.notifyDataSetChanged();
			}
		});
		
		return vv;
	}
	
	private void sendRequest(final String tid){
		new Thread(new Runnable() {
			public void run() {
				// 发送一个handler消息，叫其显示进度对话框
				Message msg = new Message();
				
				try {
					 SharedPreferences sp=context.getSharedPreferences("date",Context.MODE_PRIVATE);
					int TeacherID = sp.getInt("id",0);
					
					// 可以理解为打开一个浏览器
					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost("http://192.168.43.92:8080/SuperPerson/Teacher_TeamServlet");
					// 放置请求参数的集合
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair("tid", tid));
					params.add(new BasicNameValuePair("teacherId", String.valueOf(TeacherID)));
					Log.i("rr",tid+"------"+TeacherID);
					// 将参数统一进行编码
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
							params, "utf-8");
					post.setEntity(entity);
					HttpResponse res = client.execute(post);// 执行访问请求
					int code = res.getStatusLine().getStatusCode();		
					Log.i("rr","------"+code);
					if (code == 200) {			
						HttpEntity en = res.getEntity();
						String result = EntityUtils.toString(en,"utf-8");
						
						
						msg.obj= result;
						msg.what=11;
						handler.sendMessage(msg);
					} else {
						
					}

				} catch (Exception e) {
					e.printStackTrace();
					Log.i("ee","ss");
				}
			}

			private SharedPreferences getSharedPreferences(String string,
					String modePrivate) {
				// TODO Auto-generated method stub
				return null;
			}
		}).start();
	}
	
}
