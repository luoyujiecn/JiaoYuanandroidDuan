package adapter;

import java.util.List;

import com.example.teacher.R;


import entity.Student;
import entity.Team;
import android.content.Context;
import android.support.v4.app.INotificationSideChannel.Stub;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class StuInfoAdapter extends BaseAdapter{
	private List<Student> stus;
	private Context context;
	public StuInfoAdapter(List<Student> stus, Context context) {
		super();
		this.stus = stus;
		this.context = context;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return stus == null ? 0 : stus.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Student stu = stus.get(position);
		//��
		 final View v = LayoutInflater.from(context).inflate(R.layout.activity_student_info_item, null);
		 TextView tv_stuId_item=(TextView) v.findViewById(R.id.tv_stuId_item);
		 TextView tv_stuName_item=(TextView) v.findViewById(R.id.tv_stuName_item);
		 

		 
		 
		 Log.i("hh",stu.getRealName());
		 String RealName = stu.getRealName();
		 String LogId= stu.getLoginId();
		tv_stuName_item.setText(RealName);
		tv_stuId_item.setText(LogId);
		
		return v;
	}
	
}
