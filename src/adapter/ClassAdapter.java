package adapter;



import java.util.List;

import com.example.teacher.R;
import com.example.teacher.R.id;
import com.example.teacher.R.layout;

import entity.Team;




import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ClassAdapter extends BaseAdapter{
	private List<Team> teams;
	private Context context;
	public ClassAdapter(List<Team> teams, Context context) {
		super();
		this.teams = teams;
		this.context = context;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return teams == null ? 0 : teams.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Team team = teams.get(position);
		View v = LayoutInflater.from(context).inflate(R.layout.list_team, null);
		TextView tv_list_team=(TextView) v.findViewById(R.id.list_team); 
		tv_list_team.setText(team.getTeamName());
		ClassAdapter.this.notifyDataSetChanged();
		return v;
	}

}
