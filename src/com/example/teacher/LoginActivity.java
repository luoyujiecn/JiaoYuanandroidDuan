package com.example.teacher;

import java.util.ArrayList;


import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import close.BaseActivity;

import com.alibaba.fastjson.JSON;

import entity.TechnicalTeacher;



import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends BaseActivity {
	private EditText namexml;
	private EditText passwordxml;
	String getname=null;
    String getpassword=null;
    String name;
    String password;
   
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        namexml = (EditText) findViewById(R.id.username);
        passwordxml=(EditText) findViewById(R.id.password);
         name = namexml.getText().toString();
         password = passwordxml.getText().toString();
        //从本地文件获取用户名密码
        SharedPreferences sp=getSharedPreferences("date",MODE_PRIVATE);
        	  getname = sp.getString("name", null);
              getpassword=sp.getString("password", null);
              if(getname!=null&&getpassword!=null){
            	  Intent intent = new Intent(this,MainActivity.class);
              	  startActivity(intent); 
              	  finish();
              }         	       
        }       
    private void requestWindowFeature(int[] focusedStateSet) {
		// TODO Auto-generated method stub
		
	}
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			String json =  msg.obj.toString();
			
			TechnicalTeacher t = JSON.parseObject(json,TechnicalTeacher.class);
			String key=t.getKey();
			if("1".equals(key)){
				 name = namexml.getText().toString();
		         password = passwordxml.getText().toString();
		    	 SharedPreferences sp = getSharedPreferences("date",MODE_PRIVATE);
		         Editor editor = sp.edit();
		         editor.putInt("id", t.getTtid());
		         
		         editor.putString("name",name);
		         editor.putString("password", password);
		         editor.commit();
		         Intent intent = new Intent(LoginActivity.this,MainActivity.class);
		    	   startActivity(intent);
		    	   finish();
			} 
			else{
				Toast.makeText(LoginActivity.this, "请输入正确的用户名和密码！", Toast.LENGTH_SHORT).show();
				}
		};
	};
    
    public void denglu(View v){
    	name = namexml.getText().toString();
    	password = passwordxml.getText().toString();
         new Thread(new Runnable() {  			
   			@Override
   			public void run() {
   				
   				try{// TODO Auto-generated method stub
   				HttpClient client = new DefaultHttpClient();
   				HttpPost post = new HttpPost("http://192.168.43.92:8080/SuperPerson/Json"); 
   				//参数集合
   				List<NameValuePair> params = new ArrayList<NameValuePair>();
   				params.add(new BasicNameValuePair("username",name));
   				params.add(new BasicNameValuePair("password", password));
   				//将参数同意设置utf-8编码
   				UrlEncodedFormEntity encodedFormEntity = new UrlEncodedFormEntity(params,"utf-8");
   				post.setEntity(encodedFormEntity);
   				//执行post请求
   				HttpResponse response = client.execute(post);
   				int code = response.getStatusLine().getStatusCode();
   				if(code==200){
   					HttpEntity entity = response.getEntity();
   					String result = EntityUtils.toString(entity,"utf-8");
   					Log.i("lyj","==="+result);
   					Message msg = new Message();
   					msg.obj= result;
   					handler.sendMessage(msg);
   				}
   				}catch(Exception e){
   					e.printStackTrace();
   				}
   				
   			}
   		}).start();
    }
   
}
