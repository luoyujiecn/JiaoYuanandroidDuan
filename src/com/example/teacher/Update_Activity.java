package com.example.teacher;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import close.ActivityPool;
import close.BaseActivity;

import com.alibaba.fastjson.JSON;

import entity.TechnicalTeacher;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Update_Activity extends BaseActivity {
	private EditText olderpsd;
	private EditText newpsd_onexml;
	private EditText newpsd_twoxml;
	private String older_pssword;
	private String new_password_one;
	private String new_password_two;
	String psd;
	String name;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update);
		
		//找到输入框的id
		olderpsd=(EditText) findViewById(R.id.olderpsd);
		newpsd_onexml=(EditText) findViewById(R.id.newpsd_one);
		newpsd_twoxml=(EditText) findViewById(R.id.newpsd_two);
		//获取输入的内容
		
		
	}
	//handler从服务器获取回来的验证
	 private Handler handler = new Handler() {
			public void handleMessage(android.os.Message msg) {
				String json =  msg.obj.toString();
				
				TechnicalTeacher t = JSON.parseObject(json,TechnicalTeacher.class);
				String key=t.getKey();
				
				
				if("1".equals(key)){
			    	 SharedPreferences sp = getSharedPreferences("date",MODE_PRIVATE);
			         Editor editor = sp.edit();
			         editor.clear();
			         editor.commit(); 
			    	 Toast.makeText(Update_Activity.this, "修改密码成功！",Toast.LENGTH_SHORT).show();
			    	 ActivityPool.closeAllActivity();
			    	 Intent intent = new Intent(Update_Activity.this,LoginActivity.class);
			    	 startActivity(intent);
			    	
				} 
				else{
					Toast.makeText(Update_Activity.this, "修改密码失败", Toast.LENGTH_SHORT).show();
					}
			};
		};
	//点击button进行密码修改
		
		
	public void update(View v){
		older_pssword=olderpsd.getText().toString();
		new_password_one=newpsd_onexml.getText().toString();
		new_password_two=newpsd_twoxml.getText().toString();
		//从本地文件读取密码
		 SharedPreferences sp=getSharedPreferences("date",MODE_PRIVATE);
        psd=sp.getString("password", null);
        name = sp.getString("name", null);
        
        Log.i("tt",older_pssword+"---"+psd);
       
        if(older_pssword!=null&&new_password_one!=null&&new_password_two!=null){
        
       	 if(older_pssword.equals(psd)&&new_password_one.equals(new_password_two)){
       		 //在这里传数据到服务器
       		new Thread(new Runnable() {  			
       			@Override
       			public void run() {
       				
       				try{// TODO Auto-generated method stub
       				HttpClient client = new DefaultHttpClient();
       				HttpPost post = new HttpPost("http://192.168.43.92:8080/SuperPerson/update"); 
       				//参数集合
       				List<NameValuePair> params = new ArrayList<NameValuePair>();
		         	params.add(new BasicNameValuePair("name", name));
       				params.add(new BasicNameValuePair("update", new_password_one));
       				//将参数同意设置utf-8编码
       				UrlEncodedFormEntity encodedFormEntity = new UrlEncodedFormEntity(params,"utf-8");
       				post.setEntity(encodedFormEntity);
       				//执行post请求
       				HttpResponse response = client.execute(post);
       				int code = response.getStatusLine().getStatusCode();
       				if(code==200){
       					HttpEntity entity = response.getEntity();
       					String result = EntityUtils.toString(entity,"utf-8");
       					
       					Message msg = new Message();
       					msg.obj= result;
       					handler.sendMessage(msg);
       				}
       				}catch(Exception e){
       					e.printStackTrace();
       				}
       				
       			}
       		}).start();
       		 
       		 
       	 }else{
       		 Toast.makeText(Update_Activity.this, "原密码不正确", Toast.LENGTH_SHORT).show();
       	 }
       	 
        }else {
			Toast.makeText(Update_Activity.this, "请正确输入", Toast.LENGTH_SHORT).show();
        }
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
