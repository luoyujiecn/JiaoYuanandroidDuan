package com.example.teacher;

import java.util.ArrayList;





import java.util.List;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.example.teacher.R.id;



import entity.Team;
import entity.TechnicalTeacher;
import adapter.ClassAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;

import android.os.Message;
import android.util.Log;

import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.Window;

import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	protected static final int List = 0;
	protected static final int Team = 0;
	private ListView list;
	private TextView tv_add;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
			

		tv_add=(TextView) findViewById(R.id.tv_add);
		getAllteam();
		tv_add.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupMenu(v);
			}
		});
		list=(ListView) findViewById(R.id.list);
	
		
		
	}
	
	 
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getAllteam();
	}
	List<Team> teams;
	String teamName = "";
	int TeamId = 0;
	private Handler handler =new Handler(){
		
		public void handleMessage(android.os.Message msg) {
			// 将服务器获取到的数据设置给listView
			
			
			if(msg.what==11){
			 teams = (List<Team>)msg.obj;
			ClassAdapter adapter = new ClassAdapter(teams,MainActivity.this);
			list.setAdapter(adapter);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					
					if(position>=0){
						Intent intent = new Intent(MainActivity.this,StudentInfoActivity.class);
						/*String teamName = teams.get(position).getId();*/
						 teamName = teams.get(position).getTeamName();
						intent.putExtra("teamName",teamName);
						Toast.makeText(MainActivity.this, teams.get(position).getTeamName(), Toast.LENGTH_SHORT).show();
						startActivity(intent);
					}
				}
			});
			
			list.setOnCreateContextMenuListener(new OnCreateContextMenuListener() {
				
				@Override
				public void onCreateContextMenu(ContextMenu menu, View v,
						ContextMenuInfo menuInfo) {
					// TODO Auto-generated method stub
					 menu.setHeaderTitle("选择操作");  
		                menu.add(0, 0, 0, "取消该关联");  
		               
				}
			});
			}//这里结束msg.what==11的代码
			if(msg.what==12){
				String json =  msg.obj.toString();
				TechnicalTeacher tt = JSON.parseObject(json,TechnicalTeacher.class);
				if("1".equals(tt.getKey())){
					Toast.makeText(MainActivity.this, "解除班级关联成功", Toast.LENGTH_SHORT).show();
					
				}else if("0".equals(tt.getKey())){
					Toast.makeText(MainActivity.this, "解除班级关联失败", Toast.LENGTH_SHORT).show();
				}
			}
			
		};
	};
	
	
	//给长按按钮菜单项添加事件  
    @Override  
    public boolean onContextItemSelected(MenuItem item) {  
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();  
        final int position = info.position;
        //info.id得到listview中选择的条目绑定的id  
           
        switch (item.getItemId()) {  
        case 0:  
        	
        	AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this); 
    		
    		
    		builder.setTitle("友情提示");
    		builder.setMessage("是否确认解除班级关联？");
			builder.setNegativeButton("确认", new AlertDialog.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					 teamName = teams.get(position).getTeamName();
		        	 TeamId=teams.get(position).getTid();
		        	 Toast.makeText(MainActivity.this, "  "+TeamId, Toast.LENGTH_SHORT).show();
		        	//是不是关联班级的？对
		        	 //因为关联班级是新线程，所以，我们应该这个线程获取完毕后再刷新哈。  soga
		        	 //所以我们的刷新应该在下面那方法执行难完毕后刷新   嗯 明白
		        	 getTeamId(TeamId);
		        	
				}
			} );
    		builder.setPositiveButton("取消",null);
    		
    		builder.show();
        	
        	
        	
        	
           // updateDialog(id);  //更新事件的方法  
        	
            return true;  
          
        default:  
            return super.onContextItemSelected(item);  
        }  
    } 
	
	public void getTeamId(final int TeamId){
		new Thread(new Runnable() {
			
					@Override
					public void run() {
						
						try{// TODO Auto-generated method stub
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost("http://192.168.43.92:8080/SuperPerson/JCGLServlet"); 
						//参数集合
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("TeamId",String.valueOf(TeamId)));
						
						//将参数同意设置utf-8编码
						UrlEncodedFormEntity encodedFormEntity = new UrlEncodedFormEntity(params,"utf-8");
						post.setEntity(encodedFormEntity);
						//执行post请求
						HttpResponse response = client.execute(post);
						
						int code = response.getStatusLine().getStatusCode();
						Log.i("aa",String.valueOf(code)+"---"+TeamId+"");
						   if(code==200){
							HttpEntity entity = response.getEntity();
							String result = EntityUtils.toString(entity,"utf-8");
							
							Message msg = new Message();
							msg.obj= result;
							msg.what=12;
							handler.sendMessage(msg);
						}
						}catch(Exception e){
							e.printStackTrace();
						}
						
					}
				}).start();
	}
	
	
	/**
	 * 访问服务器
	 */
	public void getAllteam(){
		// 新开一个线程
				new Thread(new Runnable() {
					public void run() {
						// 发送一个handler消息，叫其显示进度对话框
						Message msg = new Message();

						try {
							// 可以理解为打开一个浏览器
							HttpClient client = new DefaultHttpClient();
							HttpPost post = new HttpPost("http://192.168.43.92:8080/SuperPerson/TeamServlet");
							// 放置请求参数的集合
							List<NameValuePair> params = new ArrayList<NameValuePair>();
							// 将参数统一进行编码
							UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
									params, "utf-8");
							post.setEntity(entity);
							HttpResponse res = client.execute(post);// 执行访问请求
							int code = res.getStatusLine().getStatusCode();
							if (code == 200) {
								HttpEntity en = res.getEntity();
								// 拿到返回值的字符串
								String result = EntityUtils.toString(en,"utf-8");
								
								// 将服务器返回的json转成Status对象
								List<Team> ss = JSONArray.parseArray(result,
										Team.class);
								msg.what=11;
								msg.obj = ss;// 将获取到的数据设置给obj
								handler.sendMessage(msg);
							} else {
							}
							// 是不是可以了？    这里是再获取一次班级信息，不就是刷新了列表吗？  是的 
							//那就是可以了。  ＮＯＩＫ试试　　好的
							getAllteam();

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).start();
	}
	
	/**
	 * 右上角菜单的下拉
	 * @param view
	 */
	private void showPopupMenu(View view) {
		// View当前PopupMenu显示的相对View的位置
		PopupMenu popupMenu = new PopupMenu(this, view);
		// menu布局
		popupMenu.getMenuInflater().inflate(R.menu.list,
				popupMenu.getMenu());
		// menu的item点击事件
		popupMenu
				.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						Toast.makeText(getApplicationContext(),
								item.getTitle(), Toast.LENGTH_SHORT).show();
						return false;
					}
				});
		popupMenu
				.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
						case id.one:
							Intent intent = new Intent(MainActivity.this,
									Update_Activity.class);
							MainActivity.this.startActivity(intent);
							break;
						case id.two:
							Intent intent1 = new Intent(MainActivity.this,
									Tc_Team_Activity.class);
							MainActivity.this.startActivity(intent1);
							break;
						case id.three:
							Intent intent2 = new Intent(MainActivity.this,
									null);
							MainActivity.this.startActivity(intent2);
							break;

						}

						return true;
					}
				});
		popupMenu.show();
	}
	
}
