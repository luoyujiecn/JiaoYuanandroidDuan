package com.example.teacher;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import close.ActivityPool;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

import entity.Student;
import entity.Team;
import entity.TechnicalTeacher;

import adapter.GLAdapater;
import adapter.StuInfoAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IInterface;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class StudentInfoActivity extends Activity {
	String teamName;
	private ListView list;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student_info);
		list=(ListView) findViewById(R.id.stu_info_list);
		Intent intent = getIntent();
		 teamName = intent.getStringExtra("teamName");
		
		//Toast.makeText(StudentInfoActivity.this, teamName, Toast.LENGTH_SHORT).show();
		getAllStu();
	}
	
	
	
	 private Handler handler = new Handler() {
			public void handleMessage(android.os.Message msg) {
				final List<Student> stus = (List<Student>)msg.obj;
				
				StuInfoAdapter adapter = new StuInfoAdapter(stus,StudentInfoActivity.this);
				list.setAdapter(adapter);
				list.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						
						
						if(position>=0){
							Intent intent = new Intent(StudentInfoActivity.this,DetailActivity.class);
							String RealName = stus.get(position).getRealName();
							String LogId = stus.get(position).getLoginId();
							intent.putExtra("RealName", RealName);
							intent.putExtra("LogId", LogId);
							startActivity(intent);
						}
						
					}
				});
			};
		};
	
	
	
	public void getAllStu(){
    	
         new Thread(new Runnable() {  			
   			@Override
   			public void run() {
   				Message msg = new Message();
   				try{// TODO Auto-generated method stub
   				HttpClient client = new DefaultHttpClient();
   				HttpPost post = new HttpPost("http://192.168.43.92:8080/SuperPerson/StuInfoServlet"); 
   				//参数集合
   				List<NameValuePair> params = new ArrayList<NameValuePair>();
   				params.add(new BasicNameValuePair("teamName",teamName));
   				
   				//将参数同意设置utf-8编码
   				UrlEncodedFormEntity encodedFormEntity = new UrlEncodedFormEntity(params,"utf-8");
   				post.setEntity(encodedFormEntity);
   				//执行post请求
   				HttpResponse response = client.execute(post);
   				int code = response.getStatusLine().getStatusCode();
   				
   				if(code==200){
   					HttpEntity en = response.getEntity();
					// 拿到返回值的字符串
					String result = EntityUtils.toString(en,"utf-8");
					// 将服务器返回的json转成Status对象
					List<Student> ss = JSONArray.parseArray(result,
							Student.class);
				
					
					msg.obj = ss;// 将获取到的数据设置给obj
					
					handler.sendMessage(msg);
   				}
   				}catch(Exception e){
   					e.printStackTrace();
   				}
   				
   			}
   		}).start();
    }
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.student_info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
