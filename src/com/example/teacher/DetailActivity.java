package com.example.teacher;

import android.app.Activity;
import android.app.AlertDialog;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.widget.TextView;

public class DetailActivity extends Activity {
	private TextView tv_name;
	private TextView tv_stuId;
	private TextView tv_psscore;
	private TextView tv_ksscore;
	private Button psscore;
	private Button ksscore;

	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		tv_name=(TextView) findViewById(R.id.tv_name);
		tv_stuId=(TextView) findViewById(R.id.tv_xuehao);
		tv_psscore=(TextView) findViewById(R.id.tv_psscore);
		tv_ksscore=(TextView) findViewById(R.id.tv_ksscore);
		
		psscore=(Button) findViewById(R.id.clk_update_psscore);
		ksscore=(Button) findViewById(R.id.clk_update_ksscore);
		
		
		Intent intent = getIntent();
		String RealName = intent.getStringExtra("RealName");
		String LogId = intent.getStringExtra("LogId");
		tv_name.setText(RealName);
		tv_stuId.setText(LogId);
		
		
				// TODO Auto-generated method stub
				
		

	}

	public void clk_psscore(View v){
		AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this); 
		final EditText edt= new EditText(DetailActivity.this);
		builder.setView(edt);
		builder.setTitle("修改平时成绩");
		builder.setPositiveButton("取消",null);
		builder.setNegativeButton("确认", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				String score = edt.getText().toString();
				tv_psscore.setText(score);
			}
	
		});
		builder.show();
	}
	public void clk_ksscore(View v){
		AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this); 
		final EditText edt= new EditText(DetailActivity.this);
		builder.setView(edt);
		builder.setTitle("修改平时成绩");
		builder.setPositiveButton("取消",null);
		builder.setNegativeButton("确认", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				String score = edt.getText().toString();
				tv_ksscore.setText(score);
			}
	
		});
		builder.show();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
