package com.example.teacher;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

import entity.Team;
import entity.TechnicalTeacher;
import adapter.ClassAdapter;
import adapter.GLAdapater;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;
import android.widget.Toast;

public class Tc_Team_Activity extends Activity {
	private ListView list;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tc__team);
		
		list=(ListView) findViewById(R.id.list_guanlian);
		getAllteam();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tc__team_, menu);
		return true;
	}
	
	
	private Handler handler =new Handler(){
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			int what = msg.what;
			
			String result = msg.obj.toString();
			if(what == 1){
				
				// 将服务器获取到的数据设置给listView	
				List<Team> teams = (List<Team>)msg.obj;
				GLAdapater adapter = new GLAdapater(teams,Tc_Team_Activity.this);
				adapter.setHandler(handler);
				list.setAdapter(adapter);
			}else if(what == 11){// 来自关联班级的响应信息
				
				String json =  msg.obj.toString();
				
				TechnicalTeacher t = JSON.parseObject(json,TechnicalTeacher.class);
				String key=t.getKey();
				if("1".equals(key)){
					Toast.makeText(Tc_Team_Activity.this, "关联班级成功", Toast.LENGTH_SHORT).show();
					finish();
				}else{
					Toast.makeText(Tc_Team_Activity.this, "关联班级失败", Toast.LENGTH_SHORT).show();
				}
				
			}else if(what == 12){
				
				
			}
		};
	};
	
	
	public void getAllteam(){
		// 新开一个线程
		
				new Thread(new Runnable() {
					public void run() {		
						// 发送一个handler消息，叫其显示进度对话框
						Message msg = new Message();
						try {
							// 可以理解为打开一个浏览器
							HttpClient client = new DefaultHttpClient();
							HttpPost post = new HttpPost("http://192.168.43.92:8080/SuperPerson/GuanLiServlet");
							// 放置请求参数的集合
							List<NameValuePair> params = new ArrayList<NameValuePair>();
							// 将参数统一进行编码
							UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
									params, "utf-8");
							post.setEntity(entity);
							HttpResponse res = client.execute(post);// 执行访问请求
							int code = res.getStatusLine().getStatusCode();						
							if (code == 200) {			
								HttpEntity en = res.getEntity();
								// 拿到返回值的字符串
								String result = EntityUtils.toString(en,"utf-8");
								// 将服务器返回的json转成Status对象
								List<Team> ss = JSONArray.parseArray(result,
										Team.class);
								
								msg.obj = ss;// 将获取到的数据设置给obj
								msg.what = 1;
								handler.sendMessage(msg);
							} else {
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).start();
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
